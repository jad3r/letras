Olha, não sou daqui
Me diga onde estou
Não há tempo não há nada
Que me faça sair daqui
Mas sem parar pra dançar
Sigo pistas,sigo estradas pra me perder

Nunca sei o que se passa
Com as manias do local
Porque sempre parto antes que comece a gostar
De ser igual, qualquer um
Me sentir mais uma peça no final
Cometendo um bug crítico, decimal

Na verdade continuo sob a mesma condição
Distraindo a vaidade, enganando a emoção

Pelas minhas trilhas você perde a noção
Não há placa nem pessoas informando aonde vão
Penso outra vez estou sem meus amigos
E retomo a porta aberta dos perigos

Na verdade continuo sob a mesma condição
Distraindo a verdade, enganando o coração

Na verdade continuo na mesma coisa...