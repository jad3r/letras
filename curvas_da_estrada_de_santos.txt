Se você pretende saber como eu sou
Eu posso lhe dizer
Entre no meu carro, na estrada de Santos
E você vai me assustar

Você vai pensar que eu não gosto nem mesmo de mim
E que na minha idade
Só a velocidade, anda junto a mim

Só ando sozinho, e no meu caminho
O tempo é cada vez menor
Preciso de ajuda, por favor me cuida
Eu vivo muito só

Se acaso numa curva eu me lembro do meu mundo
Eu piso mais fundo
Corrijo num segundo, não posso parar

Eu prefiro as retas da estrada de São Paulo

Mas se o amor que eu perdi, eu novamente encontrar
As curvas se acabam e na estrada de Santos
Eu não vou mais passar
Eu prefiro as curvas da estrada de Santos
Onde eu tento…