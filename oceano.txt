Assim que o dia anoiteceu
Lá no mar raso da paixão
Dava pra ver o tempo fluir
Cadê tu? Que solidão!
Esquecera de mim?

Enfim, de tudo o que há na Terra
Não há nada em lugar algum
Que vá minguar sem você chegar
Perto de ti tudo andou
Todos sabem o que eu sofri

Amar é um deserto e seus tremores
Vida que vai na sela desses odores
Não sabe voltar, me dá teu calor

Vem me fazer feliz porque eu te amo
Você deságua em mim, e eu, oceano
Me esqueço que amar é quase uma cor
Só sei viver se for longe de você!